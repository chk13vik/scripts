import os
import shutil 
import time
import os.path
 
_REPO = 'C:\\Repository\\controls\\'

f = open(_REPO + 'Nuspec\\version.txt','r+')
content = f.readline()
#print(content)

currentVersion = content.split('.')
size = len(currentVersion)
#print(currentVersion[size-1])

newVer = int(currentVersion[size-1]) 
newVer += 1
#print(newVer)
newVerStr = ''
for i in range(len(currentVersion)):
    if i == size - 1: newVerStr += str(newVer)
    else: newVerStr += (currentVersion[i] + ".")

f.seek(0)
f.truncate(0)
f.write(newVerStr)
f.close()

name = 'Intecom.Common.Controls'
f = open(_REPO + 'Nuspec\\' + name + '.nuspec','r+')
nuspec = f.readlines()
#print(nuspec[4])
nuspec[4] = '    <version>' + newVerStr + '</version>\n'
f.seek(0)
f.truncate(0)
tmp = ''.join(nuspec)
#print(tmp)
f.write(tmp)
f.close()

specSource = _REPO + 'Nuspec\\' + name
fileSource = _REPO + '.nuget\\'
os.chdir(fileSource)
reult = os.system(fileSource + 'NuGet.exe pack ' + specSource + '.nuspec')
newName = name + '.' + newVerStr + '.nupkg'
while os.path.isfile(fileSource + newName) == False:
    time.sleep(1)
    

dest = 'c:\\NuGet\\'
shutil.copyfile(fileSource + newName, dest + newName)


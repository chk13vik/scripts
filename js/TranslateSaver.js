let storage = [];

document.addEventListener('keydown', function(event) {
  if (event.code == 'KeyA') {
	let string = '';
    let word = document.getElementById('lleo_text'); 
	string += word.innerText + '\t--\t';
	let transcription = document.getElementById('lleo_transcription');
	string += transcription.innerText + "\t\t";
	let list = document.getElementsByClassName('ll-translation-item');
	
	list = Array.from(list);
	let translations = list.map((x) => x.innerText ).join(',');
	string += translations;
	console.log(string);
	
	let row = [word.innerText, transcription.innerText, translations];
	
	storage.push(row);
  }
  else if (event.code == 'KeyY' && event.ctrlKey && event.altKey)
  {
	let isBoss = confirm("базаришь?");
	if (isBoss) save(); 
	storage = [];
  }
});

let save = function(){
	var pom = document.createElement('a');
	var BOM = "\uFEFF"; 
	var csvContent = BOM + storage.join('\n');
	var blob = new Blob([csvContent],{type: 'text/csv;charset=utf-8;'});
	var url = URL.createObjectURL(blob);
	pom.href = url;
	pom.setAttribute('download', 'foo.csv');
	pom.click();
}

window.onbeforeunload = function (e) {

	if (storage.length == 0) return;

	var e = e || window.event;
	var msg = "Do you really want to leave this page?"

	// For IE and Firefox
	if (e) {
		e.returnValue = msg;
	}

	// For Safari / chrome
	return msg;
 };